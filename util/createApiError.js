function ApiError(message, statusCode, extended) {
  Error.call(this);
  Error.captureStackTrace(this);
  this.name = "ApiError";
  this.message = message;
  this.statusCode = statusCode;
  this.extended = extended;
}

function createApiError(message, statusCode, extended = {}) {
  return new ApiError(message, statusCode, extended);
}

module.exports = createApiError;
