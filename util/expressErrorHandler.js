function expressErrorHandler(error, req, res, next) {
  const isApiError = error.name === "ApiError";
  if (isApiError) {
    const { name, statusCode, message, extended } = error;
    return res.status(statusCode).json({
      name,
      message,
      extended
    });
  }

  console.error(error);
  res.status(500).json({
    name: "ServerError",
    message: `
      Something went wrong on the server. 
      Be assured that we are looking into the matter.
      Check back in some time to continue.
    `
  });
}

module.exports = expressErrorHandler;
