### Requirements

1. Requires NodeJS (tested on version `10.16.0`)
2. Requires Nginx

## Setup instructions

1. Copy `.env.example` to `.env` (in project root)
2. Change any values that may be relevant to your environment
3. Create JWT signing certificates in `./certificates` (project root). Steps for the same have been outlined below
4. Make an entry in `/etc/hosts` pointing to your choice of local domain. By default, this app assumes `http://localdev.com`
5. Create a new Nginx config for the local domain of your choice. You can use the sample config provided below. If you've changed default ports or domain name, make sure these are reflected in this configuration.
6. Run `npm install`
7. Run `npm start`
8. Your server should be up and running on the port you specified
9. You may also access the server via `http://localdev.com/api` (assuming default configuration).

#### Creating signing certificates for JWT

1. Create folder at project root: `certificates`
2. `cd certificates`
3. `openssl ecparam -genkey -name secp521r1 -out jwt-private-key.pem`
4. `openssl ec -in jwt-private-key.pem -pubout -out jwt-public-key.pem`

#### Sample Nginx configuration file

```
server {
  listen       80;
  server_name  localdev.com;

  access_log   /var/log/nginx/localdev.com.access_log;
  error_log    /var/log/nginx/localdev.com.error_log;

  location / {
    proxy_pass http://localhost:3000;
    proxy_set_header   X-Forwarded-For $remote_addr;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    #proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

  location /static/ {
    proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    #proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

  location /sockjs-node/ {
    proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  location /api/ {
    proxy_pass http://localhost:4044/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
}

```
