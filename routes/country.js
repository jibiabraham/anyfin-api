const express = require("express");
const flatten = require("lodash/flatten");
const uniq = require("lodash/uniq");
const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("../jwt");

function makeRouter(countryRepository, currencyRepository) {
  const router = express.Router();

  router.get("/name/:name", async (req, res, next) => {
    const { name } = req.params;
    const { baseCurrencyCode = "SEK" } = req.query;

    let matches;
    try {
      matches = await countryRepository.findMatchingCountriesByName(name);
    } catch (ex) {
      return next(ex);
    }

    let currencyConversionMap = {};
    if (matches.length > 0) {
      const toCurrencyCodes = uniq(
        flatten(
          matches.map(({ currencies }) => currencies.map(({ code }) => code))
        )
      );
      const amountInBaseCurrency = 1;
      try {
        const convertedAmounts = await currencyRepository.convertAmount(
          baseCurrencyCode,
          toCurrencyCodes,
          amountInBaseCurrency
        );
        for (const conversion of convertedAmounts) {
          const code = Object.keys(conversion)[0];
          currencyConversionMap[code] = conversion[code];
        }
      } catch (ex) {
        return next(ex);
      }
    }

    res.json({
      data: {
        matches,
        currencyConversionMap: {
          baseCurrencyCode,
          baseCurrencyAmount: 1,
          values: currencyConversionMap
        }
      }
    });
  });

  return router;
}

module.exports = makeRouter;
