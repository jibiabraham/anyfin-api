const express = require("express");
const isString = require("lodash/isString");
const {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
  decodeToken
} = require("../jwt");
const createApiError = require("../util/createApiError");

function makeRoutes(userRepository, tokenRepository) {
  const router = express.Router();

  router.post("/login", async (req, res, next) => {
    const { userName } = req.body;
    const isValidUserName = isString(userName) && userName.length > 0;

    if (!isValidUserName) {
      const error = createApiError("Invalid API parameters", 422, {
        userName: `Username cannot be empty`
      });
      return next(error);
    }

    try {
      await userRepository.addUser(userName);
    } catch (ex) {
      return next(ex);
    }

    const oneDayInMs = 1 * 24 * 60 * 60 * 1000;
    const accessToken = await signAccessToken({ sub: userName });
    const refreshToken = await signRefreshToken({ sub: userName });
    const {
      sub: accessTokenUserName,
      exp: expInSecondsFromEpoch
    } = await decodeToken(accessToken);

    const accessTokenExpiresAt = new Date(expInSecondsFromEpoch * 1000);
    const refreshTokenExpiresAt = new Date(Date.now() + oneDayInMs);

    res
      .cookie("refreshToken", refreshToken, {
        httpOnly: true,
        maxAge: oneDayInMs
      })
      .cookie("refreshTokenExpiresAt", refreshTokenExpiresAt.getTime(), {
        maxAge: oneDayInMs
      })
      .cookie("accessToken", accessToken, {
        expires: accessTokenExpiresAt
      })
      .cookie("accessTokenExpiresAt", accessTokenExpiresAt.getTime(), {
        expires: accessTokenExpiresAt
      })
      .cookie("accessTokenUserName", accessTokenUserName, {
        maxAge: oneDayInMs
      })
      .json({
        data: {
          accessToken,
          accessTokenUserName,
          accessTokenExpiresAt: expInSecondsFromEpoch * 1000
        }
      });
  });

  router.post("/logout", async (req, res, next) => {
    // Revoke any valid attached refresh token
    const refreshToken = req.cookies.refreshToken;
    if (refreshToken) {
      const decoded = await verifyRefreshToken(refreshToken);
      if (decoded) {
        try {
          const { jwtId, exp } = decoded;
          await tokenRepository.revokeToken(jwtId, exp);
        } catch (ex) {
          // Do not error out
          // Log this issue and continue logging out
          console.error(ex);
        }
      }
    }

    res
      .clearCookie("refreshToken", { httpOnly: true })
      .clearCookie("refreshTokenExpiresAt")
      .clearCookie("accessToken")
      .clearCookie("accessTokenExpiresAt")
      .clearCookie("accessTokenUserName")
      .json({});
  });

  return router;
}

module.exports = makeRoutes;
