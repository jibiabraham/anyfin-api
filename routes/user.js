const express = require("express");
const flatten = require("lodash/flatten");
const uniq = require("lodash/uniq");
const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("../jwt");
const createApiError = require("../util/createApiError");

function makeRouter(countryRepository, userRepository, currencyRepository) {
  const router = express.Router();

  router.post("/add-country/:fullName", async (req, res, next) => {
    const {
      user: { sub: userName }
    } = res.locals;
    const { fullName } = req.params;

    try {
      const isValidCountryName = await countryRepository.isValidCountryName(
        fullName
      );
      if (!isValidCountryName) {
        const error = createApiError("Invalid API parameters", 422, {
          fullName: `${fullName} is not a valid country full name.`
        });
        return next(error);
      }
    } catch (ex) {
      return next(ex);
    }

    let user;
    try {
      await userRepository.addCountryToUser(userName, fullName);
      user = await userRepository.getUser(userName);
    } catch (ex) {
      return next(ex);
    }

    res.json({});
  });

  router.post("/remove-country/:fullName", async (req, res, next) => {
    const {
      user: { sub: userName }
    } = res.locals;
    const { fullName } = req.params;

    try {
      const isValidCountryName = await countryRepository.isValidCountryName(
        fullName
      );
      if (!isValidCountryName) {
        const error = createApiError("Invalid API parameters", 422, {
          fullName: `${fullName} is not a valid country full name.`
        });
        return next(error);
      }
    } catch (ex) {
      return next(ex);
    }

    let user;
    try {
      await userRepository.removeCountryFromUser(userName, fullName);
    } catch (ex) {
      return next(ex);
    }

    res.json({});
  });

  router.get("/me/:baseCurrencyCode", async (req, res, next) => {
    const { baseCurrencyCode = "SEK" } = req.params;
    const {
      user: { sub: userName }
    } = res.locals;

    let user;
    try {
      user = await userRepository.getUser(userName);
    } catch (ex) {
      return next(ex);
    }

    let currencyConversionMap = {};
    if (user && user.countries && user.countries.length > 0) {
      const toCurrencyCodes = uniq(
        flatten(
          user.countries.map(({ currencies }) =>
            currencies.map(({ code }) => code)
          )
        )
      );
      const amountInBaseCurrency = 1;
      try {
        const convertedAmounts = await currencyRepository.convertAmount(
          baseCurrencyCode,
          toCurrencyCodes,
          amountInBaseCurrency
        );
        for (const conversion of convertedAmounts) {
          const code = Object.keys(conversion)[0];
          currencyConversionMap[code] = conversion[code];
        }
      } catch (ex) {
        return next(ex);
      }
    }

    res.json({
      data: {
        user: {
          ...user,
          currencyConversionMap: {
            baseCurrencyCode,
            baseCurrencyAmount: 1,
            values: currencyConversionMap
          }
        }
      }
    });
  });

  return router;
}

module.exports = makeRouter;
