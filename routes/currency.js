const express = require("express");
const isNaN = require("lodash/isNaN");
const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("../jwt");
const createApiError = require("../util/createApiError");

function makeRouter(currencyRepository) {
  const router = express.Router();

  router.get("/convert", async (req, res, next) => {
    const { baseCurrencyCode, toCurrencyCodes, amount } = req.query;

    try {
      const isValidBaseCurrency = await currencyRepository.isValidCurrencyCode(
        baseCurrencyCode
      );
      if (!isValidBaseCurrency) {
        const error = createApiError("Invalid API parameters", 422, {
          baseCurrencyCode: `${baseCurrencyCode} is not a valid currency code`
        });
        return next(error);
      }
    } catch (ex) {
      return next(ex);
    }

    try {
      const isValidToCurrencyCodes = await Promise.all(
        toCurrencyCodes.map(code =>
          currencyRepository.isValidCurrencyCode(code)
        )
      );
      const allAreValid = isValidToCurrencyCodes.every(
        isValid => isValid === true
      );
      if (!allAreValid) {
        const invalidCodes = toCurrencyCodes
          .filter((code, idx) => !isValidToCurrencyCodes[idx])
          .reduce((acc, code) => {
            acc[code] = `${code} is not a valid currency code`;
            return acc;
          }, {});
        const error = createApiError(
          "Invalid API parameters",
          422,
          invalidCodes
        );
        return next(error);
      }
    } catch (ex) {
      return next(ex);
    }

    const numericAmount = +parseFloat(amount).toFixed(2);
    const isValidAmount = !isNaN(numericAmount) && numericAmount > 0;
    if (!isValidAmount) {
      const error = createApiError("Invalid API parameters", 422, {
        amount: "Amount must be a number greater than zero"
      });
      return next(error);
    }

    let convertedAmounts;
    try {
      convertedAmounts = await currencyRepository.convertAmount(
        baseCurrencyCode,
        toCurrencyCodes,
        numericAmount
      );
    } catch (ex) {
      return next(ex);
    }

    return res.json({
      data: {
        baseCurrencyCode,
        baseCurrencyAmount: numericAmount,
        convertedAmounts
      }
    });
  });

  return router;
}

module.exports = makeRouter;
