require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const expressErrorHandler = require("../util/expressErrorHandler");
const makeRoute = require("./auth");
const request = require("supertest");
const { extractCookies } = require("../util/extractCookies");
const { signRefreshToken } = require("../jwt");
const COOKIE_SECRET = process.env.EXPRESS_SECRET;

function makeApp() {
  const app = express();
  app.use(bodyParser.json());
  app.use(cors());
  app.use(cookieParser(COOKIE_SECRET));
  return app;
}

const userName = "Testing User";
const userRepository = {
  addUser: jest.fn()
};
const tokenRepository = {
  revokeToken: jest.fn()
};
const route = makeRoute(userRepository, tokenRepository);
const app = makeApp();
app.use("/auth", route);
app.use(expressErrorHandler);

describe("Test /auth route", () => {
  test("Login without username fails with ApiError", async () => {
    const response = await request(app)
      .post("/auth/login")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({});

    expect(response.body).toMatchSnapshot();
  });

  test("Login with username returns with access token details, and sets refresh token as cookie", async () => {
    const response = await request(app)
      .post("/auth/login")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send({ userName });

    const { data } = response.body;
    expect(Object.keys(data)).toMatchSnapshot();

    const cookies = extractCookies(response.headers);
    expect(Object.keys(cookies)).toMatchSnapshot();
  });

  test("Logout clears all cookies, and revokes any valid refresh token", async () => {
    const validRefreshToken = await signRefreshToken({ sub: userName });
    const response = await request(app)
      .post("/auth/logout")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Cookie", [`refreshToken=${validRefreshToken}`]);

    expect(response.headers["set-cookie"]).toMatchSnapshot();
    expect(tokenRepository.revokeToken.mock.calls.length).toBe(1);
  });
});
