require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("../jwt");
const expressErrorHandler = require("../util/expressErrorHandler");
const makeRoute = require("./user");
const request = require("supertest");
const { extractCookies } = require("../util/extractCookies");
const { signRefreshToken, signAccessToken, decodeToken } = require("../jwt");
const makeUserRepository = require("../repositories/user");
const COOKIE_SECRET = process.env.EXPRESS_SECRET;

function makeApp() {
  const app = express();
  app.use(bodyParser.json());
  app.use(cors());
  app.use(cookieParser(COOKIE_SECRET));
  return app;
}

const userName = "Testing User";

const COUNTRIES = [
  {
    name: "TestCountry_1",
    population: 100,
    flag: "",
    currencies: [{ name: "TestCurrency_1", code: "TC1" }]
  },
  {
    name: "TestCountry_2",
    population: 100,
    flag: "",
    currencies: [{ name: "TestCurrency_2", code: "TC2" }]
  }
];
const countryRepository = {
  findMatchingCountriesByName: jest.fn(() => COUNTRIES),
  isValidCountryName: jest.fn(name => name !== "invalid"),
  findCountryByName: jest.fn(
    name => COUNTRIES.filter(country => country.name === name)[0]
  )
};

const INITIAL_USER_DATABASE_STATE = {};
const userRepository = makeUserRepository(
  INITIAL_USER_DATABASE_STATE,
  countryRepository
);

const currencyRepository = {
  convertAmount: jest.fn((baseCurrencyCode, toCurrencyCodes, amount) => {
    return toCurrencyCodes.map(code => ({ [code]: amount }));
  })
};
const tokenRepository = {
  revokeToken: jest.fn(),
  isRevokedToken: jest.fn(() => false)
};

const route = makeRoute(
  countryRepository,
  userRepository,
  currencyRepository,
  tokenRepository
);
const app = makeApp();
app.use(
  "/v1/user",
  expressSetUserFromAccessToken(),
  expressVerifyRefreshTokenIfNoAccessToken(tokenRepository),
  expressReIssueTokensAndSetUser(tokenRepository),
  route
);
app.use(expressErrorHandler);

describe("Test /v1/user route", () => {
  test("/ without auth cookies fails with ApiError", async () => {
    const response = await request(app)
      .get("/v1/user")
      .set("Accept", "application/json");

    expect(response.body).toMatchSnapshot();
  });

  test("/add-country/:fullName with valid access token cookie succeeds", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const TestCountry_1 = "TestCountry_1";
    const response = await request(app)
      .post(`/v1/user/add-country/${TestCountry_1}`)
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    const userData = await userRepository.getUser(userName);
    expect(countryRepository.isValidCountryName.mock.calls).toEqual(
      expect.arrayContaining([[TestCountry_1]])
    );
    expect(userData).toMatchSnapshot();
  });

  test("/add-country/:fullName with invalid country, fails with ApiError", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const InvalidCountryName = "invalid";
    const response = await request(app)
      .post(`/v1/user/add-country/${InvalidCountryName}`)
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    expect(response.body).toMatchSnapshot();
    expect(countryRepository.isValidCountryName.mock.calls).toEqual(
      expect.arrayContaining([[InvalidCountryName]])
    );
  });

  test("/me/:baseCurrencyCode with valid access token cookie succeeds", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const response = await request(app)
      .get(`/v1/user/me/SEK`)
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    expect(response.body).toMatchSnapshot();
  });

  test("/remove-country/:fullName with valid access token cookie succeeds", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const TestCountry_1 = "TestCountry_1";
    const response = await request(app)
      .post(`/v1/user/remove-country/${TestCountry_1}`)
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    const userData = await userRepository.getUser(userName);
    expect(countryRepository.isValidCountryName.mock.calls).toEqual(
      expect.arrayContaining([[TestCountry_1]])
    );
    expect(userData).toMatchSnapshot();
  });

  test("/remove-country/:fullName with invalid country, fails with ApiError", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const InvalidCountryName = "invalid";
    const response = await request(app)
      .post(`/v1/user/remove-country/${InvalidCountryName}`)
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    expect(response.body).toMatchSnapshot();
    expect(countryRepository.isValidCountryName.mock.calls).toEqual(
      expect.arrayContaining([[InvalidCountryName]])
    );
  });
});
