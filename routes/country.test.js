require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("../jwt");
const expressErrorHandler = require("../util/expressErrorHandler");
const makeRoute = require("./country");
const request = require("supertest");
const { extractCookies } = require("../util/extractCookies");
const { signRefreshToken, signAccessToken, decodeToken } = require("../jwt");
const COOKIE_SECRET = process.env.EXPRESS_SECRET;

function makeApp() {
  const app = express();
  app.use(bodyParser.json());
  app.use(cors());
  app.use(cookieParser(COOKIE_SECRET));
  return app;
}

const userName = "Testing User";
const countryRepository = {
  findMatchingCountriesByName: jest.fn(() => [
    {
      name: "TestCountry_1",
      population: 100,
      flag: "",
      currencies: [{ name: "TestCurrency_1", code: "TC1" }]
    },
    {
      name: "TestCountry_2",
      population: 100,
      flag: "",
      currencies: [{ name: "TestCurrency_2", code: "TC2" }]
    }
  ])
};
const currencyRepository = {
  convertAmount: jest.fn((baseCurrencyCode, toCurrencyCodes, amount) => {
    return toCurrencyCodes.map(code => ({ [code]: amount }));
  })
};
const tokenRepository = {
  revokeToken: jest.fn(),
  isRevokedToken: jest.fn(() => false)
};

const route = makeRoute(countryRepository, currencyRepository);
const app = makeApp();
app.use(
  "/v1/country",
  expressSetUserFromAccessToken(),
  expressVerifyRefreshTokenIfNoAccessToken(tokenRepository),
  expressReIssueTokensAndSetUser(tokenRepository),
  route
);
app.use(expressErrorHandler);

describe("Test /v1/country route", () => {
  test("/ without auth cookies fails with ApiError", async () => {
    const response = await request(app)
      .get("/v1/country")
      .set("Accept", "application/json");

    expect(response.body).toMatchSnapshot();
  });

  test("/name/:name with valid access token cookie succeeds", async () => {
    const validAccessToken = await signAccessToken({ sub: userName });

    const response = await request(app)
      .get("/v1/country/name/swe?baseCurrencyCode=SEK")
      .set("Accept", "application/json")
      .set("Cookie", [`accessToken=${validAccessToken}`]);

    expect(response.body).toMatchSnapshot();
    expect(
      countryRepository.findMatchingCountriesByName.mock.calls
    ).toMatchSnapshot();
    expect(currencyRepository.convertAmount.mock.calls).toMatchSnapshot();
  });

  test("/name/:name with invalid access token, but valid refresh token cookie succeeds", async () => {
    const validRefreshToken = await signRefreshToken({ sub: userName });
    const { jwtId: oldRefreshTokenId } = await decodeToken(validRefreshToken);

    const response = await request(app)
      .get("/v1/country/name/swe?baseCurrencyCode=SEK")
      .set("Accept", "application/json")
      .set("Cookie", [`refreshToken=${validRefreshToken}`]);

    const cookies = extractCookies(response.headers);
    const newAccessToken = cookies.accessToken.value;
    const newRefreshToken = cookies.refreshToken.value;
    const { jwtId: newRefreshTokenId } = await decodeToken(newRefreshToken);

    expect(response.body).toMatchSnapshot();
    expect(tokenRepository.isRevokedToken.mock.calls).toEqual(
      expect.arrayContaining([[oldRefreshTokenId]])
    );
    expect(tokenRepository.revokeToken.mock.calls).toEqual(
      expect.arrayContaining([[oldRefreshTokenId]])
    );
    expect(newAccessToken).toBeDefined();
    expect(newRefreshToken).toBeDefined();
    expect(newRefreshTokenId).toEqual(
      expect.not.stringMatching(oldRefreshTokenId)
    );
  });
});
