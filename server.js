const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const rateLimit = require("express-rate-limit");

const makeAuthRoutes = require("./routes/auth");
const makeCountryRoutes = require("./routes/country");
const makeCurrencyRoutes = require("./routes/currency");
const makeUserRoutes = require("./routes/user");
const makeCountryRepository = require("./repositories/country");
const makeCurrencyRepository = require("./repositories/fixer");
const makeUserRepository = require("./repositories/user");
const makeTokenRepository = require("./repositories/token");

const {
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser
} = require("./jwt");
const createApiError = require("./util/createApiError");
const expressErrorHandler = require("./util/expressErrorHandler");

const SERVER_PORT = process.env.PORT;
const IS_PRODUCTION = process.env.NODE_ENV === "production";
const COOKIE_SECRET = process.env.EXPRESS_SECRET;
const FIXER_API_ACCESS_KEY = process.env.FIXER_API_ACCESS_KEY;
const INITIAL_USER_DATABASE_STATE = {};
const INITIAL_TOKEN_DATABASE_STATE = { revoked: {} };

const countryRepository = makeCountryRepository();
const currencyRepository = makeCurrencyRepository(FIXER_API_ACCESS_KEY);
const userRepository = makeUserRepository(
  INITIAL_USER_DATABASE_STATE,
  countryRepository
);
const tokenRepository = makeTokenRepository(INITIAL_TOKEN_DATABASE_STATE);

const expressRateLimit = rateLimit({
  windowMs: 1 * 60 * 1000,
  max: 30,
  skipFailedRequests: true,
  keyGenerator: (req, res) => res.locals.user.jwtId,
  skip: (req, res) => !res.locals.user,
  handler: (req, res, next) => {
    const { resetTime } = req.rateLimit;
    const resetDurationMs = resetTime.getTime() - Date.now();
    const error = createApiError("Too many requests", 429, {
      resetDurationMs
    });
    next(error);
  }
});

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser(COOKIE_SECRET));

// TODO: Use secure cookies in production
if (IS_PRODUCTION) {
  app.set("trust proxy", 1);
}

app.use("/auth", makeAuthRoutes(userRepository, tokenRepository));
app.use(
  "/v1",
  expressSetUserFromAccessToken(),
  expressVerifyRefreshTokenIfNoAccessToken(tokenRepository),
  expressReIssueTokensAndSetUser(tokenRepository),
  expressRateLimit
);

// TODO: Rate limit
app.use(
  "/v1/country",
  makeCountryRoutes(countryRepository, currencyRepository)
);
app.use("/v1/currency", makeCurrencyRoutes(currencyRepository));
app.use(
  "/v1/user",
  makeUserRoutes(countryRepository, userRepository, currencyRepository)
);

// Error handler
app.use(expressErrorHandler);

app.listen(SERVER_PORT, () => {
  console.log(`Server is running on port ${SERVER_PORT}`);
});
