"use strict";
const fs = require("fs");
const path = require("path");
const { promisify } = require("util");
const uuidv4 = require("uuid/v4");
const jwt = require("jsonwebtoken");
const createApiError = require("../util/createApiError");

const jwtSign = promisify(jwt.sign);
const jwtVerify = promisify(jwt.verify);

const JWT_PRIVATE_KEY = fs.readFileSync(
  path.resolve(__dirname, "../certificates/jwt-private-key.pem")
);
const JWT_PUBLIC_KEY = fs.readFileSync(
  path.resolve(__dirname, "../certificates/jwt-public-key.pem")
);
const JWT_ACCESS_TOKEN_AUDIENCE = process.env.JWT_ACCESS_TOKEN_AUDIENCE;
const JWT_REFRESH_TOKEN_AUDIENCE = process.env.JWT_REFRESH_TOKEN_AUDIENCE;
const JWT_ISSUER = process.env.JWT_ISSUER;

const DEFAULT_JWT_OPTIONS = {
  algorithm: "ES512",
  issuer: JWT_ISSUER
};

async function sign(payload, provided_options = {}) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    ...provided_options
  };
  const payloadWithJwtId = {
    ...payload,
    jwtId: uuidv4()
  };
  return await jwtSign(payloadWithJwtId, JWT_PRIVATE_KEY, options);
}
async function signAccessToken(payload) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    expiresIn: "15m",
    audience: JWT_ACCESS_TOKEN_AUDIENCE
  };
  return await sign(payload, options);
}
async function signRefreshToken(payload) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    expiresIn: "1d",
    audience: JWT_REFRESH_TOKEN_AUDIENCE
  };
  return await sign(payload, options);
}

async function verify(token, provided_options = {}) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    ...provided_options
  };
  try {
    // Will return decoded payload
    return await jwtVerify(token, JWT_PUBLIC_KEY, options);
  } catch (ex) {
    console.error(ex);
    return false;
  }
}
async function verifyAccessToken(token) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    audience: JWT_ACCESS_TOKEN_AUDIENCE
  };
  return await verify(token, options);
}
async function verifyRefreshToken(token) {
  const options = {
    ...DEFAULT_JWT_OPTIONS,
    audience: JWT_REFRESH_TOKEN_AUDIENCE
  };
  return await verify(token, options);
}

function expressSetUserFromAccessToken() {
  return async function(req, res, next) {
    let accessToken;
    const hasAuthorizationHeader = !!req.headers.authorization;
    const hasAuthorizationCookie = !!req.cookies.accessToken;
    if (hasAuthorizationHeader) {
      const authHeader = (req.headers.authorization || "").split("Bearer");
      accessToken = authHeader && authHeader[1] && authHeader[1].trim();
    } else if (hasAuthorizationCookie) {
      accessToken = req.cookies.accessToken;
    }

    let decodedAccessToken;
    if (accessToken) {
      decodedAccessToken = await verifyAccessToken(accessToken);
    }

    if (decodedAccessToken) {
      res.locals.user = decodedAccessToken;
    }

    next();
  };
}

function expressVerifyRefreshTokenIfNoAccessToken(tokenRepository) {
  return async function(req, res, next) {
    if (res.locals.user) {
      return next();
    }

    const refreshToken = req.cookies.refreshToken;
    let decodedRefreshToken = null;
    if (refreshToken) {
      decodedRefreshToken = await verifyRefreshToken(refreshToken);
    }

    let isRevokedToken = false;
    if (decodedRefreshToken) {
      const { jwtId } = decodedRefreshToken;
      try {
        isRevokedToken = await tokenRepository.isRevokedToken(jwtId);
      } catch (ex) {
        return next(ex);
      }
    }

    if (!isRevokedToken) {
      res.locals.refreshToken = decodedRefreshToken;
    }

    next();
  };
}

function expressReIssueTokensAndSetUser(tokenRepository) {
  const errorMessage = `Request does not include a valid token`;

  return async function(req, res, next) {
    if (res.locals.user) {
      return next();
    }

    if (!res.locals.user && !res.locals.refreshToken) {
      const error = createApiError("Invalid Token", 403, {
        message: errorMessage
      });
      return next(error);
    }

    const { sub, jwtId } = res.locals.refreshToken;

    try {
      await tokenRepository.revokeToken(jwtId);
    } catch (ex) {
      // Is there a graceful way to handle this?
      throw ex;
    }

    const oneDayInMs = 1 * 24 * 60 * 60 * 1000;
    const newAccessToken = await signAccessToken({ sub });
    const newRefreshToken = await signRefreshToken({ sub });

    const newDecodedAccessToken = await decodeToken(newAccessToken);
    const newDecodedRefreshToken = await decodeToken(newRefreshToken);
    const newAccessTokenExpiresAt = new Date(newDecodedAccessToken.exp * 1000);

    res
      .cookie("refreshToken", newRefreshToken, {
        httpOnly: true,
        maxAge: oneDayInMs
      })
      .cookie("refreshTokenExpiresAt", newDecodedRefreshToken.exp * 1000, {
        maxAge: oneDayInMs
      })
      .cookie("accessToken", newAccessToken, {
        expires: newAccessTokenExpiresAt
      })
      .cookie("accessTokenExpiresAt", newDecodedAccessToken.exp * 1000, {
        expires: newAccessTokenExpiresAt
      })
      .cookie("accessTokenUserName", sub + newDecodedAccessToken.exp, {
        expires: newAccessTokenExpiresAt
      })
      .cookie("accessTokenUserName", sub, {
        maxAge: oneDayInMs
      });

    res.locals.user = newDecodedAccessToken;
    next();
  };
}

async function decodeToken(token) {
  return jwt.decode(token);
}

module.exports = {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
  expressSetUserFromAccessToken,
  expressVerifyRefreshTokenIfNoAccessToken,
  expressReIssueTokensAndSetUser,
  decodeToken
};
