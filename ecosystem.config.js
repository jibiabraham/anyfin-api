module.exports = {
  apps: [
    {
      name: "API Server",
      script: "server.js",
      instances: "max",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      exec_interpreter: "node@10.16.0",
      node_args: "-r dotenv/config",
      time: true,
      env: {
        NODE_ENV: "production"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ],

  deploy: {
    production: {
      user: "deploy",
      host: "anyfin.ordinary.systems",
      ref: "origin/master",
      repo: "git@gitlab.com:jibiabraham/anyfin-api.git",
      path: "/home/deploy/api_server"
    }
  }
};
