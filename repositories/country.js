const fetch = require("node-fetch");
const isString = require("lodash/isString");

const URL_BASE = "https://restcountries.eu/rest/v2";
const URL_ALL = `${URL_BASE}/all`;

class RestCountries {
  constructor() {
    this.requestDataCache = [];
  }

  async makeRequest(url, options = {}) {
    const urlString = url.toString();

    // Check if data in cache
    // If so, return cached data
    const cachedData = this.requestDataCache[urlString];
    if (cachedData) {
      return cachedData;
    }

    const response = await fetch(url, options);
    // A new set of data is available
    if (response.status === 200) {
      // Update cache values
      this.requestDataCache[urlString] = await response.json();
    } else {
      // TODO: Handle all other known statuses
    }

    return this.requestDataCache[urlString];
  }

  async getAllCountries() {
    const url = new URL(URL_ALL);
    url.searchParams.set("fields", "name;population;currencies;flag");
    const countries = await this.makeRequest(url.toString());
    return countries;
  }

  async findCountryByName(name) {
    const countries = await this.getAllCountries();
    const matches = countries.filter(country => country.name === name);
    return matches[0];
  }
  async findMatchingCountriesByName(name) {
    const countries = await this.getAllCountries();
    const nameRegex = new RegExp(name, "ig");
    const matches = countries.filter(country => nameRegex.test(country.name));
    return matches;
  }

  async isValidCountryName(fullName) {
    const countries = await this.getAllCountries();
    const isValid =
      isString(fullName) &&
      fullName.length > 0 &&
      countries.some(country => country.name === fullName);
    return isValid;
  }
}

module.exports = () => new RestCountries();
