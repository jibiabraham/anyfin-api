class User {
  constructor(initialState, countryRepository) {
    this.data = initialState;
    this.countryRepository = countryRepository;
  }

  async addUser(name) {
    if (!(name in this.data)) {
      this.data[name] = { countries: [] };
    }
    return this.data[name];
  }

  async addCountryToUser(userName, fullCountryName) {
    let user = this.data[userName];
    if (!user) {
      user = await this.addUser(userName);
    }
    const existsInList = user.countries.some(name => name === fullCountryName);
    if (!existsInList) {
      user.countries = [fullCountryName, ...user.countries];
    }
    return user;
  }

  async removeCountryFromUser(userName, fullCountryName) {
    let user = this.data[userName];
    if (!user) {
      user = await this.addUser(userName);
    }
    user.countries = user.countries.filter(name => name !== fullCountryName);
    return user;
  }

  // TODO: Add pagination for countries
  async getUser(userName) {
    const user = this.data[userName];
    if (!user) {
      return null;
    }

    const countries = await Promise.all(
      user.countries.map(countryFullName =>
        this.countryRepository.findCountryByName(countryFullName)
      )
    );

    return {
      name: userName,
      countries
    };
  }
}

module.exports = (initialState = {}, countryRepository) =>
  new User(initialState, countryRepository);
