const fetch = require("node-fetch");
const isString = require("lodash/isString");

const URL_BASE = "http://data.fixer.io/api";
const URL_LATEST = `${URL_BASE}/latest`;

class Fixer {
  constructor(accessKey) {
    this.accessKey = accessKey;

    this.eTagCache = {};
    this.requestTimeCache = {};
    this.requestDataCache = {};
    this.requestDataCacheValidity = {};
  }

  async makeRequest(url, options = {}) {
    const urlString = url.toString();
    const prevETag = this.eTagCache[urlString];
    const prevRequestTime = this.requestTimeCache[urlString];
    const cacheHeaders = prevETag
      ? {
          "Content-Type": "application/json",
          "If-None-Match": prevETag,
          "If-Modified-Since": prevRequestTime
        }
      : { "Content-Type": "application/json" };

    const newOptions = {
      ...options,
      headers: {
        ...options.headers,
        ...cacheHeaders
      }
    };

    // Check if data in cache and cached data is still valid
    // If so, return cached data
    const now = Date.now();
    const cachedData = this.requestDataCache[urlString];
    const cacheExpiry = this.requestDataCacheValidity[urlString];
    if (cachedData && now < cacheExpiry) {
      return cachedData;
    }

    const response = await fetch(url, newOptions);
    // A new set of data is available
    if (response.status === 200) {
      // Update cache values
      const eTag = response.headers.get("etag");
      this.eTagCache[urlString] = eTag;

      const date = response.headers.get("date");
      this.requestTimeCache[urlString] = date;

      this.requestDataCache[urlString] = await response.json();
    } else {
      // Data has not been modified
      // Update request time cache
      const date = response.headers.get("date");
      this.requestTimeCache[urlString] = date;
    }
    // TODO: Handle all other known statuses

    // Update requestCacheExpiryTime
    const oneHourInMs = 1 * 60 * 60 * 1000;
    const newCacheExpiry = now + oneHourInMs;
    this.requestDataCacheValidity[urlString] = newCacheExpiry;

    return this.requestDataCache[urlString];
  }

  async getLatestRatesInBaseEuro() {
    const { accessKey } = this;

    const url = new URL(URL_LATEST);
    url.searchParams.set("access_key", accessKey);

    const rates = await this.makeRequest(url.toString());
    return rates;
  }

  async convertAmount(
    baseCurrencyCode = "SEK",
    toCurrencyCodes = [],
    amount = 0
  ) {
    const ratesWithBaseEuro = await this.getLatestRatesInBaseEuro();
    const amountInEuro = amount / ratesWithBaseEuro.rates[baseCurrencyCode];
    const amountInSpecifiedCurrencies = toCurrencyCodes.map(code => {
      const oneEuroInCurrencyUnits = ratesWithBaseEuro.rates[code];
      const amount = +(amountInEuro * oneEuroInCurrencyUnits).toPrecision(2);
      return { [code]: amount };
    });

    return amountInSpecifiedCurrencies;
  }

  async isValidCurrencyCode(currencyCode) {
    const ratesWithBaseEuro = await this.getLatestRatesInBaseEuro();
    return isString(currencyCode) && currencyCode in ratesWithBaseEuro.rates;
  }
}

module.exports = accessKey => new Fixer(accessKey);
