class Token {
  constructor(initialState) {
    this.data = initialState;
  }

  async revokeToken(id, expireAtInSecondsFromEpoch) {
    this.data.revoked[id] = expireAtInSecondsFromEpoch;
    return true;
  }

  async isRevokedToken(id) {
    const isRevoked = id in this.data.revoked;
    return isRevoked;
  }
}

module.exports = (initialState = { revoked: {} }) => new Token(initialState);
